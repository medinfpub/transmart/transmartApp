<head>
    <meta name='layout' content='main'/>
    <title>${grailsApplication.config.com.recomdata.appTitle}</title>

    <style type='text/css' media='screen'>
    #login {
        margin: 15px 0px;
        padding: 0px;
        text-align: center;
    }

    #login .inner {
        width: 260px;
        margin: 0px auto;
        text-align: left;
        padding: 10px;
        border-top: 1px dashed #499ede;
        border-bottom: 1px dashed #499ede;
        background-color: #EEF;
    }

    #login .inner .fheader {
        padding: 4px;
        margin: 3px 0px 3px 0;
        color: #2e3741;
        font-size: 14px;
        font-weight: bold;
    }

    #login .inner .cssform p {
        clear: left;
        margin: 0;
        padding: 5px 0 8px 0;
        padding-left: 105px;
        border-top: 1px dashed gray;
        margin-bottom: 10px;
        height: 1%;
    }

    #login .inner .cssform input[type='text'] {
        width: 120px;
    }

    #login .inner .cssform label {
        font-weight: bold;
        float: left;
        margin-left: -105px;
        width: 100px;
    }

    #login .inner .login_message {
        color: red;
    }

    #login .inner .text_ {
        width: 120px;
    }

    #login .inner .chk {
        height: 12px;
    }
    </style>
</head>

<body>

<div align="center" style="clear:both; margin-left:auto; margin-right:auto; text-align:center">
    <table style="width:auto; border:0px; text-align:center; margin:auto;" align="center">
        <tr>
            <td style="text-align:center;vertical-align:middle;margin-left:-40px;padding-top:20px;">
                <g:link controller="RWG" action="index"><img
                        src="${resource(dir: 'images', file: grailsApplication.config.com.recomdata.largeLogo)}"
                        alt="Transmart"/></g:link>
            </td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <tr>
            <td>
                <div style="margin-right:auto;margin-left:auto;width:435px;">
                    <div class="x-box-tl">
                        <div class="x-box-tr">
                            <div class="x-box-tc">
                            </div>
                        </div>
                    </div>
        
                    <div class="x-box-ml">
                        <div class="x-box-mr">
                            <div class="x-box-mc" style="text-align:left">
                                <h3 style="margin-bottom:20px; text-align:left; font-size:11px; color: #006dba;">
                                    Please login...
                                </h3>
                                <g:if test='${flash.message}'>
                                    <div class='login_message' style="color:red; font-size:12px;">${flash.message}</div>
                                </g:if>
                                <form action='${postUrl}' method='POST' id='loginForm' class='cssform'>
                                    <table style="border:0px; text-align:center; width:100%">
                                        <tr>
                                            <td style="width: 100px">
                                                <label for='j_username' style="font-weight:bold">Username:</label>
                                            </td>
                                            <td style="white-space:nowrap;" NOWRAP>
                                                <input type='text' class='text_' name='j_username' id='j_username'
                                                       style="width:100%" autofocus/>
                                                <script>
                                                    if (!("autofocus" in document.createElement("input"))) {
                                                        document.getElementById("j_username").focus();
                                                    }
                                                </script>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label for='j_password' style="font-weight:bold">Password:</label>
                                            </td>
                                            <td>
                                                <input type='password' class='text_' name='j_password' id='j_password'
                                                       style="width:100%"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label for='remember_me' style="font-weight:bold">Remember me:</label>
                                            </td>
                                            <td>
                                                <input type='checkbox' class='chk' name='_spring_security_remember_me' id='remember_me' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan=2 style="text-align:center">
                                                <br>
                                                <input type='submit' id='loginButton' value='Login' style="width:100%"/>
                                                <br>
                                                <br>
                                            </td>
                                        </tr>
                                        <g:if test='${grailsApplication.config.org.transmart.security.samlEnabled}'>
                                            <tr>
                                                <td colspan="2" style="font-size:10px;">
                                                    <a href="${createLink([action: 'login', controller: 'saml'])}">&gt; Federated Login</a>
                                                </td>
                                            </tr>
                                        </g:if>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>
        
                    <div class="x-box-bl">
                        <div class="x-box-br">
                            <div class="x-box-bc">
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <g:if test='${grailsApplication.config.ui.loginScreen.disclaimer}'>
            <tr>
                <td style="font-weight: bold;text-align:center;color:#CC0000;vertical-align:middle;margin-left:-40px; padding-top: 10px;">
                    <div style="margin-right:auto;margin-left:auto;width:435px">
                        ${grailsApplication.config.ui.loginScreen.disclaimer}
                     </div>
                </td>
            </tr>
        </g:if>
        <tr><td>&nbsp;</td></tr>
        <tr>
            <td style="text-align:center;vertical-align:middle;margin-left:-40px; padding-top: 10px;">
                <g:set var="providerName" value="${grailsApplication.config?.com?.recomdata?.providerName}"/>
                <g:set var="providerLogo" value="${grailsApplication.config?.com?.recomdata?.providerLogo}"/>
                <g:if test="${providerName}">
                    <a id="providerpowered" target="_blank"
                       href="${grailsApplication.config?.com?.recomdata?.providerURL}" style="text-decoration: none;">
                        <div>
                            <span style="font-size:10px;display: inline-block;line-height: 35px; height: 35px;">Powered by&nbsp;</span>
			    <g:if test="{$providerLogo}">
                                <img src="${providerLogo}" alt="${providerName}"
                                     style="height:35px;vertical-align:middle;margin-bottom: 12px;">
                            </g:if>
                            <g:else>
                               <img src="${resource(dir: 'images', file: 'provider_logo.png')}" alt="${providerName}"
                                     style="height:35px;vertical-align:middle;margin-bottom: 12px;">
                            </g:else>
                        </div>
                    </a>
                </g:if>
            </td>
        </tr>
    </table>
    <!-- SysINFLAME DISCLAIMER -->
	<div style="display: flex; justify-content: center; margin-top: 20px;font: 12px verdana, arial, helvetica, sans-serif;line-height: 1.2em;">
		<div style="margin-right: auto; margin-left: auto; width: 700px;">
			<div class="x-box-tl">
				<div class="x-box-tr">
					<div class="x-box-tc"></div>
				</div>
			</div>
			<div class="x-box-ml">
				<div class="x-box-mr">
					<div class="x-box-mc" style="text-align: left;font: 12px verdana, arial, helvetica, sans-serif;line-height: 1.2em;">
						<div style="font-weight: bold; text-align: center;color: red;padding-bottom:1.2em;" class="disclaimer">DISCLAIMER</div>
						<div style="padding: 1.2em 0;" class="sysinflame">By logging in to this
								application I acknowledge that, according to the <a
								href="http://www.dfg.de/download/pdf/dfg_im_profil/reden_stellungnahmen/download/empfehlung_wiss_praxis_1310.pdf"
								target="_blank"
								style="color: blue; text-decoration: underline">DFG-Memorandum
									on Safeguarding Good Scientific Practice</a>, I am obliged to
								ensure good scientific practice by consulting a (bio)medical
								informaticist (data management) and a statistician or
								epidemiologist (analysis) prior to publishing any results
								obtained (partly or in full) through the use of this
								application.

						</div>
						<div class="sysinflame" style="color: red;">
								Noncompliance could lead to a severe lack in
								reproducibility and may constitute a case of scientific
								misconduct.
						</div>
					</div>
				</div>
			</div>
			<div class="x-box-bl">
				<div class="x-box-br">
					<div class="x-box-bc"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- Additional Information -->
	<div style="display: flex;justify-content: center;margin-top: 30px;">
	    <div style="width:370px;">
            <div class="x-box-tl">
                <div class="x-box-tr">
                    <div class="x-box-tc">
                    </div>
                </div>
            </div>

            <div class="x-box-ml">
                <div class="x-box-mr">
                    <div class="x-box-mc" style="text-align:left;background:none">
                        <div style="font: 12px verdana, arial, helvetica, sans-serif;line-height: 1.2em;">
                            <h3>How to Log In</h3>
                            <br>Usernames are usually the inital of your first name added to your surname: <b>isurname</b>.<br> 
                            To gain access to this service, you can request an account here: <a href="mailto:${grailsApplication.config.com.recomdata.administrator}" target="_blank" style="text-decoration:underline;color:#0000FF">administrator</a><br><br><br><br><br><br><br><br>
                        </div>
                    </div>
                </div>
            </div>

            <div class="x-box-bl">
                <div class="x-box-br">
                    <div class="x-box-bc">
                    </div>
                </div>
            </div>
        </div>
        <div style="width:370px;">
            <div class="x-box-tl">
                <div class="x-box-tr">
                    <div class="x-box-tc">
                    </div>
                </div>
            </div>

            <div class="x-box-ml">
                <div class="x-box-mr">
                    <div class="x-box-mc" style="text-align:left;background:none">
                        <div style="font: 12px verdana, arial, helvetica, sans-serif;line-height: 1.2em;">
                            <h3>Access tranSMART via APIs</h3>
                            
                            Data in the tranSMART instance can be directly queried and accessed via the transMART REST API 1.0 as described <a href="https://github.com/thehyve/transmart-rest-api" target="_blank" style="text-decoration:underline;color:#0000FF">here</a>.<br/><br/>
                            
                            <b>REST API</b>: Get an access token for this instance after logging in at <a href="../oauth/authorize?response_type=code&client_id=api-client&client_secret=api-client" style="text-decoration:underline;color:#0000FF">../oauth/authorize?response_type=code&client_id=api-client&client_secret=api-client</a>. A working example Talend Open Studio implementation based on demo data on this server is available at <a href="https://gitlab.gwdg.de/medinfpub/tos/ConnectToTranSMART" target="_blank" style="text-decoration:underline;color:#0000FF">here</a>.<br/><br/>

                            <b>R API</b>: Access data directly from R. An example R script of the <a href="https://github.com/transmart/RInterface" target="_blank">RInterface</a> for this server is available <a href="https://gitlab.gwdg.de/medinfpub/transmart_rinterface_demo/blob/master/tranSMARTInterfaceDemo.R" target="_blank" style="text-decoration:underline;color:#0000FF">here</a>.
                        </div>
                    </div>
                </div>
            </div>

            <div class="x-box-bl">
                <div class="x-box-br">
                    <div class="x-box-bc">
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>
</body>
