## transmartApp


tranSMART is a knowledge management platform that enables scientists to develop
and refine research hypotheses by investigating correlations between genetic and
phenotypic data, and assessing their analytical results in the context of
published literature and other work.


### IntelliJ WAR

-SmartR (und ggf. weitere Repositorien) einstellen (https://github.com/Medical-Informatics-Goettingen/SmartR.git)

-Branch einstellen: unten git:master -> checkout revision or branch -> origin/release-16.2(-UMG)

-neues Projekt mit Git erstellen (https://github.com/Medical-Informatics-Goettingen/transmartApp.git), sollte automatisch als Grails erkannt werden

-Branch einstellen: unten git:master -> checkout revision or branch -> origin/release-16.2(-UMG)

-ganz wichtig: Java 7! (Project settings, Strg+Ums+Alt+S)

-rechtsklick root folder -> grails -> settings -> grails sdk einstellen (v2.3.11)

-Processing external build config at C:\Users\Bender_Theresa\.grails\transmartConfig\BuildConfig.groovy
```
grails.plugin.location.smartR = '../SmartR'
grails.project.dependency.resolution = {
    repositories {
        mavenRepo([
                name: 'repo.thehyve.nl-public',
                url: 'https://repo.thehyve.nl/content/repositories/public/',
        ])
    }
}
```
-Run->Run...->Edit Configurations...->Commandline: war, VM options: -Xmx1024M

##### Testen einer WAR in Docker

-zum Testen: tmf-Container (https://github.com/dennyverbeeck/transmart-docker)

-docker-compose.yml: 
```
  tmapp:
#   image: dennyverbeeck/transmart-app:etriks-v4.0
    image: tmapp
    build:
      context: ./transmart-app
```
-transmart-app/Dockerfile:
```
#RUN curl -L https://repo.etriks.org/transmart.war -o /usr/local/tomcat/webapps/transmart.war
COPY transmart.war /opt/transmart.war
RUN cp /opt/transmart.war /usr/local/tomcat/webapps/transmart.war
```
-war aus <ProjectPath>/target kopieren nach transmart-app vor docker-compose up



### Installation

Some pre-requisites are required in order to run tranSMART. For development,
a copy of [grails][1] is needed in order to run the application. For production
or evaluation purposes, it is sufficient to download a pre-build WAR file, for
instance a snapshot from the [The Hyveʼs][2] or [tranSMART Foundationʼs][3]
CI/build servers, for snapshots of The Hyveʼs or tranSMART Foundationʼs GitHub
repositories, respectively. In order to run the WAR, an application server is
required. The only supported one is [Tomcat][4], either from the 6.x or 7.x
line, though it will most likely work on others.

In addition, a PostgreSQL database installed with the proper schema and data is
required. As of this moment, The Hyveʼs development branches require this to be
propared with the [transmart-data][5] repository. This project also handles
the required configuration, running the Solr instances and, for development
purposes, running an R server and installing sample data.

For details on how to install the tranSMART Foundationʼs versions, refer to
[their wiki][6].


  [1]: http://grails.org/
  [2]: https://ci.ctmtrait.nl/
  [3]: https://ci.transmartfoundation.org/
  [4]: http://tomcat.apache.org/
  [5]: https://github.com/thehyve/transmart-data
  [6]: https://wiki.transmartfoundation.org/
